﻿using System.Collections.Generic;
using static WeaponThread.WeaponStructure;
using static WeaponThread.WeaponStructure.WeaponDefinition;
using static WeaponThread.WeaponStructure.WeaponDefinition.HardPointDef;
using static WeaponThread.WeaponStructure.WeaponDefinition.ModelAssignmentsDef;
using static WeaponThread.WeaponStructure.WeaponDefinition.HardPointDef.HardwareDef.ArmorState;
using static WeaponThread.WeaponStructure.WeaponDefinition.HardPointDef.Prediction;
using static WeaponThread.WeaponStructure.WeaponDefinition.TargetingDef.BlockTypes;
using static WeaponThread.WeaponStructure.WeaponDefinition.TargetingDef.Threat;   

namespace WeaponThread {   
    partial class Weapons {
        // Don't edit above this line
        WeaponDefinition ripper890_inline_weaponCS => new WeaponDefinition // Weapon Class ID Goes Here.
        {

            Assignments = new ModelAssignmentsDef
            {
                MountPoints = new[] {
                    new MountPointDef {
                        SubtypeId = "ripper890_inline_cube", // Your Block SubtypeID from Cubeblock Goes here.
                        AimPartId = "", 
                        MuzzlePartId = "None", // Your Subpart name, the Empty not the model, goes here. Minus subpart_ of course. Example "GunBarrel" , on model it's "subpart_GunBarrel"
                        AzimuthPartId = "None", // Your Subpart name, the Empty not the model, is this one, this handles Rotating the Turret.
                        ElevationPartId = "None", // Your Subpart name, the Empty not the model, is this one, this handles Vertical motion for the Turret.
                        DurabilityMod = 0.25f, // This modifies your blocks damage taken, this is rarely needing to be touched.
                        IconName = "TestIcon.dds" // This is used for Custom Icons, for your Inventory management screen. Optional.
                    },
                  },
                Barrels = new[] { // Your Muzzle Names, the Empties \ Nodes , not your Model name, nor subpart.
                    "muzzle_barrel_001",

                },
                Ejector = "", // Used for Advanced Functionality, of ejecting empty shells & particles, on firing.
                Scope = "camera", //Where line of sight checks are performed from must be clear of block collision
            },
            Targeting = new TargetingDef
            {
                Threats = new[] { // Targeting List; Valid are Grids, Projectiles, Characters, Meteors, 
                    Grids,
                },
                SubSystems = new[] { // Subsystem Basic priority system, leave only "any" to disable. Decoys are in Utility.
                    Thrust, Utility, Offense, Power, Production, Any,
                },
                ClosestFirst = false, // tries to pick closest targets first (blocks on grids, projectiles, etc...). Relative to Turret itself.
                IgnoreDumbProjectiles = false, // Don't fire at non-smart projectiles. 
                LockedSmartOnly = false, // Only fire at smart projectiles that are locked on to parent grid.
                MinimumDiameter = 0, // 0 = unlimited, Minimum radius of threat to engage.  Meters of Measurement.
                MaximumDiameter = 0, // 0 = unlimited, Maximum radius of threat to engage. Meters of Measurement.
                MaxTargetDistance = 0, // 0 = unlimited, Maximum target distance that targets will be automatically shot at.
                MinTargetDistance = 0, // 0 = unlimited, Min target distance that targets will be automatically shot at.
                TopTargets = 0, // 0 = unlimited, max number of top targets to randomize between.
                TopBlocks = 0, // 0 = unlimited, max number of blocks to randomize between
                StopTrackingSpeed = 0, // do not track target threats traveling faster than this speed. IE, If it moves at 10km\s, and this is set to 1k, it ain't shooting it.
            },
            HardPoint = new HardPointDef
            {
                WeaponName = "ripper890_inline", // name of weapon in terminal , Accepts Spaces , Avoid Special Characters if possible.
                DeviateShotAngle = 0.2f, // Measured in Degrees, for highest degree of inaccuracy applied to weaponsfire
                AimingTolerance = 1f, // 0 - 180 firing angle , How off-Target the Weapon can fire, if AI Controlled.
                AimLeadingPrediction = Accurate, // Off, Basic, Accurate, Advanced -- Targeting Effectiveness, higher levels improves Turret Intelligence in leading their shots.
                DelayCeaseFire = 0, // Measured in game ticks (6 = 100ms, 60 = 1 seconds, etc..). , How long after you or the Turret stops holding the fire trigger, that the weapon keeps on firing.
                AddToleranceToTracking = false,
                CanShootSubmerged = false,

                Ui = new UiDef // User Terminal Menu Options.
                {
                    RateOfFire = false, // True, if Users can lower RPM in-Game.
                    DamageModifier = false,
                    ToggleGuidance = false,
                    EnableOverload = false,
                },
                Ai = new AiDef
                { // Turret Systems. All but LockOnFocus is used by Turrets. If not Turret, set to False , outside of Specific Usage.
                    TrackTargets = false,
                    TurretAttached = false,
                    TurretController = false,
                    PrimaryTracking = false,
                    LockOnFocus = false, // System targets your Grid's locked-on target, Used  by both Turrets & other weapons.
                    SuppressFire = false, // Disables automatic fire of Turrets, useful for Gimbals.
                },
                HardWare = new HardwareDef
                { // Turret Data
                    RotateRate = 0.1f, // Speed of Motion
                    ElevateRate = 0.1f,
                    MinAzimuth = -180, // Degrees , Turrets Start at 0. Combine values, for Turrets full range of motion. -90 here, and 90 below , gives Turret 180 degrees of motion.
                    MaxAzimuth = 180,
                    MinElevation = -9,
                    MaxElevation = 50,
                    FixedOffset = false,
                    InventorySize = 1f, // Your inventory size modifier.
                    Offset = Vector(x: 0, y: 0, z: 0), // Offset to aim focus.
                    Armor = IsWeapon, // IsWeapon, Passive, Active
					//  Upgrade, means this Block ignores non-Upgrade Config Settings, etc etc.
                },
                Other = new OtherDef
                {
                    GridWeaponCap = 4, // Cap per Grid, of this Block. 
                    RotateBarrelAxis = 0, // Axis ( X, Y, Z) of Barrel Spin
                    EnergyPriority = 0, // Energy Priority, over other subsystems on grid, should Power be limited.
                    MuzzleCheck = false,
                    Debug = false, //  Used for Debugging Turrets, Please leave on False for Live Mods, this generates extra data & visual lines on all Weapons set to True.
                    RestrictionRadius = 0, // Meters, radius of sphere disable this gun if another is present
                    CheckInflatedBox = false, // if true, the bounding box of the gun is expanded by the RestrictionRadius
                    CheckForAnyWeapon = false, // if true, the check will fail if ANY gun is present, false only looks for this subtype
                },
                Loading = new LoadingDef
                {
                    RateOfFire = 300, // RPM of Weapon. Used by Barrel Spin, if used.
                    BarrelsPerShot = 1, //  Number of Barrels fired per Trigger Pull.
                    TrajectilesPerBarrel = 1, // Number of Trajectiles per barrel per fire event.
                    SkipBarrels = 0, // If Skips set number of barrels per, this is not Barrel specific.
                    ReloadTime = 300, // Measured in game ticks (6 = 100ms, 60 = 1 seconds, etc..).
                    DelayUntilFire = 0, // Measured in game ticks (6 = 100ms, 60 = 1 seconds, etc..).
                    HeatPerShot = 1, //heat generated per shot
                    MaxHeat = 70000, //max heat before weapon enters cooldown (70% of max heat)
                    Cooldown = .95f, //percent of max heat to be under to start firing again after overheat accepts .2-.95
                    HeatSinkRate = 9000000, //amount of heat lost per second
                    DegradeRof = true, // progressively lower rate of fire after 80% heat threshold (80% of max heat)
                    ShotsInBurst =  5, // Shots before Burst Delay is triggered - BarrelsPerShot Values above 1 still only cost 1 shot from BurstCount. Independent from Magazine Size.
                    DelayAfterBurst = 60, // Measured in game ticks (6 = 100ms, 60 = 1 seconds, etc..).
                    FireFullBurst = true, // If Weapon is forced to fire the entire Burst count.
                    GiveUpAfterBurst = false, //  If Turret disengages Target after firing full Burst.
                    BarrelSpinRate = 0, // visual only, 0 disables and uses RateOfFire. Use to set a fixed rate of spin, indepdendent of ROF, to allow more flexible RPM without losing visual.
                    DeterministicSpin = false, // Spin barrel position will always be relative to initial / starting positions (spin will not be as smooth).
                },
                Audio = new HardPointAudioDef
                {
                    PreFiringSound = "", // Sound used while Charging.
                    FiringSound = "ripper890_inline_soundType", // WepShipGatlingShot is example. Audio triggered per Shot.
                    FiringSoundPerShot = true, //  if False, use Looping Audio, suggested for Beam Weapons.
                    ReloadSound = "", //  Triggered when Reloading
                    NoAmmoSound = "", // Triggered when unable to reload.
                    HardPointRotationSound = "WepTurretGatlingRotate", // Used when Turret Rotates
                    BarrelRotationSound = "WepShipGatlingRotation", // Sound used by Natural Barrel Rotation Trigger
                    FireSoundEndDelay = 60, // Measured in game ticks(6 = 100ms, 60 = 1 seconds, etc..).
                },
                Graphics = new HardPointParticleDef
                {

                    Barrel1 = new ParticleDef
                    {
                        Name = "", // Smoke_LargeGunShot Example, Particle SubtypeID from particle SBCs, go here.
                        Color = Color(red: 0, green: 0, blue: 0, alpha: 1),
                        Offset = Vector(x: 0, y: -1, z: 0),

                        Extras = new ParticleOptionDef
                        {
                            Restart = false,
                            MaxDistance = 50,
                            MaxDuration = 0,
                            Scale = 1f, // This is a setting that works slightly. Please use SBC, for the rest, and more ensured results.
                        },
                    },
                    Barrel2 = new ParticleDef
                    {
                        Name = "",//Muzzle_Flash_Large
                        Color = Color(red: 0, green: 0, blue: 0, alpha: 1),
                        Offset = Vector(x: 0, y: -1, z: 0),

                        Extras = new ParticleOptionDef
                        {
                            Restart = false,
                            MaxDistance = 150,
                            MaxDuration = 0,
                            Scale = 1f,
                        },
                    },
                },
            },
            Ammos = new[] {
                ripper890_inline_ammoCS, // must list primary, shrapnel and pattern ammos. These are Ammo Class ID.
            },
            //Animations = AnimationClassID,  // De-comment this line, to enable & set your Animation CS. 
           // Upgrades = UpgradeModulesClassID, // De-Comment this line, to enable & set your valid Upgrade Modules.
            // Don't edit below this line
        };
    }
}