using static WeaponThread.WeaponStructure.WeaponDefinition;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.AmmoEjectionDef;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.AmmoEjectionDef.SpawnType;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.ShapeDef.Shapes;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.GraphicDef;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.TrajectoryDef;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.TrajectoryDef.GuidanceType;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.DamageScaleDef;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.DamageScaleDef.ShieldDef.ShieldType;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.AreaDamageDef;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.AreaDamageDef.EwarFieldsDef;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.AreaDamageDef.EwarFieldsDef.PushPullDef.Force;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.AreaDamageDef.AreaEffectType;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.GraphicDef.LineDef;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.GraphicDef.LineDef.Texture;
using static WeaponThread.WeaponStructure.WeaponDefinition.AmmoDef.GraphicDef.LineDef.TracerBaseDef;
namespace WeaponThread
{ // Don't edit above this line
    partial class Weapons
    {
        private AmmoDef weaponID_ammoCS => new AmmoDef // Class ID goes Here.
        {
            AmmoMagazine = "weaponID_magazine", // Your Magazine SubtypeID from AmmoMagazine SBC. Use "Energy", to get an Energy weapon - Energy is a magazine included in CoreParts/
            AmmoRound = "weaponID_ammoCS", // Your Ammunition's ID, when used for Shrapnel, and the In-game Display name.
            HybridRound = false, //AmmoMagazine based weapon with energy cost
            EnergyCost = 0.00000000001f, //(((EnergyCost * DefaultDamage) * ShotsPerSecond) * BarrelsPerShot) * ShotsPerBarrel
            BaseDamage = 999999999f, // Damage , Reference; A Steel Plate is 100 HP, so it is 100 Damage per Steel Plate. Light Armor on Large-Grid, is 2.5k due to 25 steel plates.
            Mass = 0f, // in kilograms , applied onto Target as Force.
            Health = 1, // 0 = disabled, otherwise how much damage it can take from other trajectiles before dying. This makes Turrets Target this projectile, and enables Flares to work on it, if Tracking-Enabled.
            BackKickForce = 0f, // Recoil Force applied to Shooter, in kilograms
            DecayPerShot = 0f, // Damage dealt to Weapon system, per shot fired. Uses same value system as Base Damage.
            HardPointUsable = true, // set to false if this is a shrapnel ammoType and you don't want the turret to be able to select it directly. You still need to put this Ammunition on the Weapon, if used as shrapnel.
            EnergyMagazineSize = 0, // Settable Magazine size, for Energy Weapons. Physical Ammo Magazines, use the Value in AmmoMagazine SBC
            IgnoreWater = false, //  Water API Interaction.

            Shape = new ShapeDef //defines the collision shape of projectile, defaults line and visual Line Length if set to 0
            {
                Shape = LineShape, // LineShape or SphereShape. Do not use SphereShape for fast moving projectiles if you care about precision: Sphere expands brutally at high speed.
                Diameter = 1, // Diameter is minimum length of LineShape or minimum diameter of SphereShape
            },
            ObjectsHit = new ObjectsHitDef
            {
                MaxObjectsHit = 0, // 0 = disabled , Manual Armor-Piercing system - is redundant, unless reducing Pen.
                CountBlocks = false, // counts gridBlocks and not just entities hit
            },
            Shrapnel = new ShrapnelDef
            {
                AmmoRound = "", //  AmmoRound ID, from your Shrapnel Ammo Definition is used here, 
                Fragments = 100, // Number of Projectiles spawned
                Degrees = 15, // Degrees of Scatter
                Reverse = false, // Invert Default Direction (Which is Forward)
                RandomizeDir = false, // randomize between forward and backward directions
            },
            Pattern = new AmmoPatternDef()
            {
                Ammos = new[] { // Advanced Functionality)
                    "", // Ammo IDs go here, for your Pattern.
                },
                Enable = false, // Enable System
                TriggerChance = 1f, // Percent  Chance of Triggering Ammo Pattern.
                Random = false, // If Listing Order is used.
                RandomMin = 1,
                RandomMax = 1,
                SkipParent = false, // If Parent Ammunition is included in Pattern.
                PatternSteps = 1, // Number of Ammos activated per round, will progress in order and loop.  Ignored if Random = true.
            },
            DamageScales = new DamageScaleDef
            {
                MaxIntegrity = 0f, // 0 = disabled, 1000 = any blocks with currently integrity above 1000 will be immune to damage.
                DamageVoxels = false, // true = voxels are vulnerable to this weapon
                SelfDamage = false, // true = allow self damage.
                HealthHitModifier = 0.5, // defaults to a value of 1, this setting modifies how much Health is subtracted from a projectile per hit (1 = per hit). HP of Projectiles, are the Health Pool referenced. 
                VoxelHitModifier = 1,
                Characters = -1f, // Multiplier of Damage. -1f is Disabled. 0f is 0 Damage Dealt to. 2f  is twice the damage.
                // modifier values: -1 = disabled (higher performance), 0 = no damage, 0.01 = 1% damage, 2 = 200% damage.
                FallOff = new FallOffDef
                {
                    Distance = 0f, // Distance at which max damage begins falling off. This is in Meters.
                    MinMultipler = 0f, // value from 0.0f to 1f where 0.1f would be a min damage of 10% of max damage.
                },
                Grids = new GridSizeDef
                {
                    Large = -1f,  // Multiplier of Damage. -1f is Disabled. 0f is 0 Damage Dealt to. 2f  is twice the damage.
                    Small = -1f,  // Multiplier of Damage. -1f is Disabled. 0f is 0 Damage Dealt to. 2f  is twice the damage.
                },
                Armor = new ArmorDef
                {
                    Armor = -1f,  // Multiplier of Damage. -1f is Disabled. 0f is 0 Damage Dealt to. 2f  is twice the damage.
                    Light = -1f,  // Multiplier of Damage. -1f is Disabled. 0f is 0 Damage Dealt to. 2f  is twice the damage.
                    Heavy = -1f,  // Multiplier of Damage. -1f is Disabled. 0f is 0 Damage Dealt to. 2f  is twice the damage.
                    NonArmor = -1f,  // Multiplier of Damage. -1f is Disabled. 0f is 0 Damage Dealt to. 2f  is twice the damage.
                },
                Shields = new ShieldDef
                {
                    Modifier = 1f,  // Multiplier of Damage. -1f is Disabled. 0f is 0 Damage Dealt to. 2f  is twice the damage.
                    Type = Energy, // Damage Type, Options are Energy, Kinetic, Emp, and Bypass. Emp Affects Shielding with debuffs & higher multiplier inherently. Bypass ignores shields, piercing through it.
					//  Energy & Kinetic, are related to Shield Resistence Settings, set by the Players In-game.
                    BypassModifier = -1f, //  Damage modifier applied to Projectile Damage, after piercing a shield. Values less than 1,  mean reducing damage if a shield has been penned.
                },
                // first true/false (ignoreOthers) will cause projectiles to pass through all blocks that do not match the custom subtypeIds.
                Custom = new CustomScalesDef
                {
                    IgnoreAllOthers = false,
                    Types = new[]
                    {
                        new CustomBlocksDef
                        {
                            SubTypeId = "Test1",
                            Modifier = -1f,
                        },
                        new CustomBlocksDef
                        {
                            SubTypeId = "Test2",
                            Modifier = -1f,
                        },
                    },
                },
            },
            AreaEffect = new AreaDamageDef
            {
                AreaEffect = Disabled, // Disabled = do not use area effect at all, Explosive, Radiant, AntiSmart, JumpNullField, JumpNullField, EnergySinkField, AnchorField, EmpField, OffenseField, NavField, DotField.
                Base = new AreaInfluence
				//  Explosive, uses Keen Explosive Fall-Off Damage, Destroys voxels
				// Radiant, has no Fall-Off Applies damage value to all objects in radius
				// Anti-Smart redirects Smart-Projectiles after it
				//  JumpNullField, depletes Jump Drive Energy, and cancels in-progress Jumps.
				// Anchor, Applies force onto target, centered towards AOE Center.
				// EmpField, applies RNG-selected effects onto blocks in radius, once depleting Block "Resistence"
				// OffenseField, Select RNG-Effecte, once Resistence of Blocks is depleted.
				// NavField, Scrambles Gyro Control, preventing Gyro usage while in effect
				// DotField, Damage-Over-Time Field, applies damage on each trigger, to affected blocks in radius.
				
                {
                    Radius = 0f, // the sphere of influence of area effects. Meters
                    EffectStrength = 0f, // For ewar it applies this amount per pulse/hit, non-ewar applies this as damage per tick per entity in area of influence. For radiant 0 == use spillover from BaseDamage, otherwise use this value.
					 // Damage , for reference; 100 HP per Steel Plate, a Light Armor block is 25 steel Plates, for 2.5k HP, 100 Damage is required to destroy 1 Steel Plate.
					 // Damage is Per pulse Trigger, when used for DOT, a quick pulse, at 100 damage is 100(x)PulseRate, check your Math to ensure you don't over-do it.
					 // Damage for Explosive has natural fall-off from blast center, Keep in Mind, and it is delivered at once.
					 // Radiant is the raw Damage value applied once, to all valid surfaces exposed to blast.
                },
                Pulse = new PulseDef // interval measured in game ticks (60 == 1 second), pulseChance chance (0 - 100) that an entity in field will be hit
                {
                    Interval = 0, // Delay Between Pulses, in Ticks. 60 = 1 Second
                    PulseChance = 0, // Chance that Grids & Blocks within radius, will be hit; 50f, means 50% chance per target, to be struck per pulse.
                    GrowTime = 0, // Time (in ticks), that Field Size takes to expand to full size.
                    HideModel = false, // Hide Model used for Field
                    ShowParticle = false, // Hide or show Particles
                    Particle = new ParticleDef
                    {
                        Name = "", //ShipWelderArc Example. Particle SubtypeID, from Particle SBC.
                        ShrinkByDistance = false,
                        Color = Color(red: 128, green: 0, blue: 0, alpha: 32),
                        Offset = Vector(x: 0, y: -1, z: 0),
                        Extras = new ParticleOptionDef
                        {
                            Loop = false,
                            Restart = false,
                            MaxDistance = 5000,
                            MaxDuration = 1,
                            Scale = 1,
                        },
                    },
                },
                Explosions = new ExplosionDef
                {
                    NoVisuals = false,
                    NoSound = false,
                    NoShrapnel = false,
                    NoDeformation = false,
                    Scale = 1,
                    CustomParticle = "",
                    CustomSound = "",
                },
                Detonation = new DetonateDef
                {
                    DetonateOnEnd = false,
                    ArmOnlyOnHit = false,
                    DetonationDamage = 0,
                    DetonationRadius = 0,
                    MinArmingTime = 0, //Min time in ticks before projectile will arm for detonation (will also affect shrapnel spawning)
                },
                EwarFields = new EwarFieldsDef
                {
                    Duration = 0, // Duration in Ticks (60 per Second) , per Stack of Debuff.
                    StackDuration = false, // if each Pulse, stacks their Debuff, combining duration.
                    Depletable = false, // If Stacks deplete
                    MaxStacks = 0, // Max Number of Stacking Effects
                    TriggerRange = 0f, // Trigger Range of AOE Pulse
                    DisableParticleEffect = true,
                    Force = new PushPullDef // AreaEffectDamage is multiplied by target mass.
                    {
                        ForceFrom = ProjectileLastPosition, // ProjectileLastPosition, ProjectileOrigin, HitPosition, TargetCenter, TargetCenterOfMass
                        ForceTo = HitPosition, // ProjectileLastPosition, ProjectileOrigin, HitPosition, TargetCenter, TargetCenterOfMass
                        Position = TargetCenterOfMass, // ProjectileLastPosition, ProjectileOrigin, HitPosition, TargetCenter, TargetCenterOfMass
                    },
                },
            },
            Beams = new BeamDef // Sets Ammo as beam
            {
                Enable = false, //  False to disable. True to Enable.
                VirtualBeams = false, // Only one hot beam, but with the effectiveness of the virtual beams combined (better performace). Hot Beams means only one deals damage.
                ConvergeBeams = false, // When using virtual beams this option visually converges the beams to the location of the real beam.
                RotateRealBeam = false, // The real (hot beam) is rotated between all virtual beams, instead of centered between them.
                OneParticle = false, // Only spawn one particle hit per beam weapon.
            },
            Trajectory = new TrajectoryDef
            {
                Guidance = None,   //  Options; Smart , None, TravelTo, DetectFixed, DetectSmart ,DetectTravelTo
				//  Smart, enables Tracking Definition area settings, allowing tracking of target(s), becomes Valid for Anti-Smart Fields, and targeting parameters detecting "Smart" projectiles.
				// None, disables Smart Definition Settings. Projectile, Beam, or similar, is considered "Dumb" by targeters.
				// TravelTo , Enables Smart Definition Settings, locks onto last known Coordinate of given Target, adjusting to strike that position
				// DetectFixed, is used by Mines, for their blast trigger.
				//  DetectSmart, is smart-tracking enabled once the Mine is triggered.
				// DetectTravelTo, is used by Mines, to travel to detected coordinates, once the mine is triggered.
				
                TargetLossDegree = 80f, // Degrees, in which Lock-On is lost, for the Projectile.
                TargetLossTime = 0, // 0 is disabled, Measured in game ticks (6 = 100ms, 60 = 1 seconds, etc..).
                MaxLifeTime = 0, // 0 is disabled, Measured in game ticks (6 = 100ms, 60 = 1 seconds, etc..).
                AccelPerSec = 0f, // Used for Missiles', starting Velocity & Rate of Velocity Change per Second.
                DesiredSpeed = 500, // voxel phasing if you go above 5100 , this is in meters per second.
                MaxTrajectory = 9500f, // Distance in Meters, that this projectile, or beam, can travel.
                FieldTime = 0, // 0 is disabled, a value causes the projectile to come to rest, spawn a field and remain for a time (Measured in game ticks, 60 = 1 second). Use this, if you want EWAR to work properly.
                GravityMultiplier = 0f, // Gravity multiplier, influences the trajectory of the projectile, value greater than 0 to enable. Does NOT work, on SMART Projectiles.
                SpeedVariance = Random(start: 0, end: 0), // subtracts value from DesiredSpeed
                RangeVariance = Random(start: 0, end: 0), // subtracts value from MaxTrajectory
                MaxTrajectoryTime = 0, // How long the weapon must fire before it reaches MaxTrajectory. This is used for Beams, typically, to make them non-hitscan. It is in ticks (60 per Second)
                Smarts = new SmartsDef
                {
                    Inaccuracy = 0f, // 0 is perfect, hit accuracy will be a random num of meters between 0 and this value.
                    Aggressiveness = 1f, // controls how responsive tracking is.   Do NOT raise above 2.
                    MaxLateralThrust = 0.5, // controls how sharp the trajectile may turn. Do NOT raise above 0.5
                    TrackingDelay = 0, // Measured in Shape diameter units traveled.
                    MaxChaseTime = 0, // Measured in game ticks (6 = 100ms, 60 = 1 seconds, etc..). 0 is Infinite.
                    OverideTarget = true, // when set to true ammo picks its own target, does not use hardpoint's.
                    MaxTargets = 0, // Number of targets allowed before ending, 0 = unlimited
                    NoTargetExpire = false, // Expire without ever having a target at TargetLossTime
                    Roam = false, // Roam current area after target loss. Please have an Life-time value if this value is set. 
                    KeepAliveAfterTargetLoss = false, // Whether to stop early death of projectile on target loss
                },
                Mines = new MinesDef
                {
                    DetectRadius = 0, //  Trigger radius of Mine, for seeking targets.
                    DeCloakRadius = 0, //  Radius in Meters, where Mine is decloaked, in proximity
                    FieldTime = 0, // Mine Lifespawn, in Ticks.
                    Cloak = false,
                    Persist = false, // If mine persists through reload  - IE , if saved while alive, it projectile remains to exist once loaded from that save. If False, Does not get saved, nor loaded.
                },
            },
            AmmoGraphics = new GraphicDef
            {
                ModelName = "", // Example "//Models//Ammo//ModelNameHere.mwm ", your Model if applicable.
                VisualProbability = 1f,
                ShieldHitDraw = false, // Shield impact, when projectile hits.
                Particles = new AmmoParticleDef
                {
                    Ammo = new ParticleDef
                    {
                        Name = "", //ShipWelderArc , Particle that is spawned on the projectile. Uses subtypeID from Particle SBC.
                        ShrinkByDistance = false,
                        Color = Color(red: 128, green: 0, blue: 0, alpha: 32),
                        Offset = Vector(x: 0, y: 0, z: 0),
                        Extras = new ParticleOptionDef
                        {
                            Restart = false,
                            MaxDistance = 5000,
                            MaxDuration = 0,
                            Scale = 1,
                        },
                    },
                    Hit = new ParticleDef //  Particle Spawned, on hitting something.
                    {
                        Name = "", // Particle SubtypeID is used here. 
                        ApplyToShield = true, // If Particle is spawned when shield is struck.
                        ShrinkByDistance = false,
                        Color = Color(red: 3, green: 1.9f, blue: 1f, alpha: 1),
                        Offset = Vector(x: 0, y: 0, z: 0),
                        Extras = new ParticleOptionDef
                        {
                            Restart = false,
                            MaxDistance = 5000,
                            MaxDuration = 0,
                            Scale = 1,
                            HitPlayChance = 1f,
                        },
                    },
                    Eject = new ParticleDef // Ejection System, for spent Shells.
                    {
                        Name = "",
                        ApplyToShield = true,
                        ShrinkByDistance = false,
                        Color = Color(red: 3, green: 1.9f, blue: 1f, alpha: 1),
                        Offset = Vector(x: 0, y: 0, z: 0),
                        Extras = new ParticleOptionDef
                        {
                            Restart = false,
                            MaxDistance = 5000,
                            MaxDuration = 30,
                            Scale = 1,
                            HitPlayChance = 1f,
                        },
                    },
                },
                Lines = new LineDef // Tracer Configuration Area
                {
                    ColorVariance = Random(start: 0.75f, end: 2f), // multiply the color by random values within range.
                    WidthVariance = Random(start: 0f, end: 0f), // adds random value to default width (negatives shrinks width)
                    Tracer = new TracerBaseDef
                    {
                        Enable = true, // If Tracer will be used.
                        Length = 5f, // Line Length \ Distance
                        Width = 0.1f, // Line Thickness. 
                        Color = Color(red: 3, green: 2, blue: 1f, alpha: 1),
                        VisualFadeStart = 0, // Number of ticks the weapon has been firing before projectiles begin to fade their color
                        VisualFadeEnd = 0, // How many ticks after fade began before it will be invisible.
                        Textures = new[] {// WeaponLaser, ProjectileTrailLine, WarpBubble, etc..
                            "ProjectileTrailLine",
                        },
                        TextureMode = Normal, // Normal, Cycle, Chaos, Wave
                        Segmentation = new SegmentDef
                        {
                            Enable = false, // If true Tracer TextureMode is ignored
                            Textures = new[] {
                                "",
                            },
                            SegmentLength = 0f, // Uses the values below.
                            SegmentGap = 0f, // Uses Tracer textures and values
                            Speed = 1f, // meters per second
                            Color = Color(red: 1, green: 2, blue: 2.5f, alpha: 1),
                            WidthMultiplier = 1f,
                            Reverse = false,
                            UseLineVariance = true,
                            WidthVariance = Random(start: 0f, end: 0f),
                            ColorVariance = Random(start: 0f, end: 0f)
                        }
                    },
                    Trail = new TrailDef
                    {
                        Enable = false, // If Trail is spawned.
                        Textures = new[] {
                            "",
                        },
                        TextureMode = Normal,
                        DecayTime = 128, //  Time in Ticks, (60 per second), that the trail exists. 120 means 2 seconds before each trail section despawns.
                        Color = Color(red: 0, green: 0, blue: 1, alpha: 1),
                        Back = false,
                        CustomWidth = 0,
                        UseWidthVariance = false, 
                        UseColorFade = true,
                    },
                    OffsetEffect = new OffsetEffectDef
                    {
                        MaxOffset = 0,// 0 offset value disables this effect
                        MinLength = 0.2f,
                        MaxLength = 3,
                    },
                },
            },
            AmmoAudio = new AmmoAudioDef // Audio, references SubtypeIDs from Audio SBC.
            {
                TravelSound = "",
                HitSound = "",
                ShieldHitSound = "",
                PlayerHitSound = "",
                VoxelHitSound = "",
                FloatingHitSound = "",
                HitPlayChance = 0.5f,
                HitPlayShield = true,
            }, // Ejection Section
            Ejection = new AmmoEjectionDef
            {
                Type = Particle, // Particle or Item (Inventory Component)
                Speed = 100f, // Speed inventory is ejected from in dummy direction
                SpawnChance = 0.5f, // chance of triggering effect (0 - 1)
                CompDef = new ComponentDef
                {
                    ItemDefinition = "", //InventoryComponent name
                    LifeTime = 0, // how long item should exist in world
                    Delay = 0, // delay in ticks after shot before ejected
                }
            },
        }; // End of Entry

		
		
		
		
		// Don't touch after this.
      }
}
