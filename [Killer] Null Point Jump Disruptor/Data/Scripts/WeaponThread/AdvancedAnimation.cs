﻿using System.Collections.Generic;
using static JDD_Mod_Thread.WeaponStructure;
using static JDD_Mod_Thread.WeaponStructure.WeaponDefinition;
using static JDD_Mod_Thread.WeaponStructure.WeaponDefinition.AnimationDef;
using static JDD_Mod_Thread.WeaponStructure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers;
using static JDD_Mod_Thread.WeaponStructure.WeaponDefinition.AnimationDef.RelMove.MoveType;
using static JDD_Mod_Thread.WeaponStructure.WeaponDefinition.AnimationDef.RelMove;
namespace JDD_Mod_Thread
{ // Don't edit above this line
    partial class Weapons
    {
        /// Possible Events ///
        
        //Reloading,
        //Firing,
        //Tracking,
        //Overheated,
        //TurnOn,
        //TurnOff,
        //BurstReload,
        //OutOfAmmo,
        //PreFire,
        //EmptyOnGameLoad,
        //StopFiring,
        //StopTracking

        private AnimationDef AdvancedAnimation => new AnimationDef
        {
            /**
                Emissives = new[]
                {
                    Emissive(
                        EmissiveName: "TurnOn",
                        Colors: new []
                        {
                            Color(red:0, green: 0, blue:0, alpha: 1),//will transitions form one color to the next if more than one
                            Color(red:0.05f, green: .021f, blue:0, alpha: .05f),
                        },
                        IntensityFrom:0, //starting intensity, can be 0.0-1.0 or 1.0-0.0, setting both from and to, to the same value will stay at that value
                        IntensityTo:1,
                        CycleEmissiveParts: false,//whether to cycle from one part to the next, while also following the Intensity Range, or set all parts at the same time to the same value
                        LeavePreviousOn: true,//true will leave last part at the last setting until end of animation, used with cycleEmissiveParts
                        EmissivePartNames: new []
                        {
                            "Emissive3"
                        }),
                    Emissive(
                        EmissiveName: "TurnOff",
                        Colors: new []
                        {
                            Color(red:0.05f, green: .021f, blue:0, alpha: .05f),
                            Color(red:0, green: 0, blue:0, alpha: 1),//will transitions form one color to the next if more than one
                        },
                        IntensityFrom:1, //starting intensity, can be 0.0-1.0 or 1.0-0.0, setting both from and to, to the same value will stay at that value
                        IntensityTo:0,
                        CycleEmissiveParts: false,//whether to cycle from one part to the next, while also following the Intensity Range, or set all parts at the same time to the same value
                        LeavePreviousOn: true,//true will leave last part at the last setting until end of animation, used with cycleEmissiveParts
                        EmissivePartNames: new []
                        {
                            "Emissive3"
                        }),
                    },
                */
            EventParticles = new Dictionary<PartAnimationSetDef.EventTriggers, EventParticle[]>
            {
            },
            
            WeaponAnimationSets = new[]
            {

                new PartAnimationSetDef
                {
                    SubpartId = Names("Inner Ring"),
                    BarrelId = "Any", //only used for firing events, use "Any" for all muzzles, muzzle triggers only this animation if not Any
                    AnimationDelays = Delays(FiringDelay : 0, ReloadingDelay: 120, OverheatedDelay: 0, TrackingDelay: 0, LockedDelay: 0, OnDelay: 0, OffDelay: 0, BurstReloadDelay: 0, OutOfAmmoDelay: 0, PreFireDelay: 0),//Delay before animation starts
                    Reverse = Events(),
                    Loop = Events(),
                    EventMoveSets = new Dictionary<PartAnimationSetDef.EventTriggers, RelMove[]>
                    {
                        [PreFire] = new[] //Firing, Reloading, etc, see Possible Events,  define a new[] for each
                        {
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 290, //number of ticks to complete motion, 60 = 1 second
                                MovementType = ExpoDecay,
                                LinearPoints = new[]
                                {
                                    Transformation(0, -2.9, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                        },
                        [Firing] = new[] //Firing, Reloading, etc, see Possible Events,  define a new[] for each
                        {
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 20, //number of ticks to complete motion, 60 = 1 second
                                MovementType = ExpoDecay,
                                LinearPoints = new[]
                                {
                                    Transformation(0, 9.2, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 40, //number of ticks to complete motion, 60 = 1 second
                                MovementType = Delay,
                                LinearPoints = new[]
                                {
                                    Transformation(0, 0, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 60, //number of ticks to complete motion, 60 = 1 second
                                MovementType = ExpoDecay,
                                LinearPoints = new[]
                                {
                                    Transformation(0, -6.4, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                        },
                        [Reloading] = new[] //Firing, Reloading, etc, see Possible Events,  define a new[] for each
                        {
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 540, //number of ticks to complete motion, 60 = 1 second
                                MovementType = Linear,
                                LinearPoints = new[]
                                {
                                    Transformation(0, -2.9, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 540, //number of ticks to complete motion, 60 = 1 second
                                MovementType = Linear,
                                LinearPoints = new[]
                                {
                                    Transformation(0, 2.9, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                        },
                    }
                },
                new PartAnimationSetDef
                {
                    SubpartId = Names("Middle Ring"),
                    BarrelId = "Any", //only used for firing events, use "Any" for all muzzles, muzzle triggers only this animation if not Any
                    AnimationDelays = Delays(FiringDelay : 0, ReloadingDelay: 120, OverheatedDelay: 0, TrackingDelay: 0, LockedDelay: 0, OnDelay: 0, OffDelay: 0, BurstReloadDelay: 0, OutOfAmmoDelay: 0, PreFireDelay: 0),//Delay before animation starts
                    Reverse = Events(),
                    Loop = Events(),
                    EventMoveSets = new Dictionary<PartAnimationSetDef.EventTriggers, RelMove[]>
                    {
                        [PreFire] = new[] //Firing, Reloading, etc, see Possible Events,  define a new[] for each
                        {
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 290, //number of ticks to complete motion, 60 = 1 second
                                MovementType = ExpoDecay,
                                LinearPoints = new[]
                                {
                                    Transformation(0, -4, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                        },
                        [Firing] = new[] //Firing, Reloading, etc, see Possible Events,  define a new[] for each
                        {
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 20, //number of ticks to complete motion, 60 = 1 second
                                MovementType = ExpoDecay,
                                LinearPoints = new[]
                                {
                                    Transformation(0, 7, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 40, //number of ticks to complete motion, 60 = 1 second
                                MovementType = Delay,
                                LinearPoints = new[]
                                {
                                    Transformation(0, 0, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 60, //number of ticks to complete motion, 60 = 1 second
                                MovementType = ExpoDecay,
                                LinearPoints = new[]
                                {
                                    Transformation(0, -3, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                        },
                        [Reloading] = new[] //Firing, Reloading, etc, see Possible Events,  define a new[] for each
                        {
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 540, //number of ticks to complete motion, 60 = 1 second
                                MovementType = Linear,
                                LinearPoints = new[]
                                {
                                    Transformation(0, -4, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 540, //number of ticks to complete motion, 60 = 1 second
                                MovementType = Linear,
                                LinearPoints = new[]
                                {
                                    Transformation(0, 4, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                        },
                    }
                },
                new PartAnimationSetDef
                {
                    SubpartId = Names("Outter Ring"),
                    BarrelId = "Any", //only used for firing events, use "Any" for all muzzles, muzzle triggers only this animation if not Any
                    AnimationDelays = Delays(FiringDelay : 0, ReloadingDelay: 120, OverheatedDelay: 0, TrackingDelay: 0, LockedDelay: 0, OnDelay: 0, OffDelay: 0, BurstReloadDelay: 0, OutOfAmmoDelay: 0, PreFireDelay: 0),//Delay before animation starts
                    Reverse = Events(),
                    Loop = Events(),
                    EventMoveSets = new Dictionary<PartAnimationSetDef.EventTriggers, RelMove[]>
                    {
                        [PreFire] = new[] //Firing, Reloading, etc, see Possible Events,  define a new[] for each
                        {
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 290, //number of ticks to complete motion, 60 = 1 second
                                MovementType = ExpoDecay,
                                LinearPoints = new[]
                                {
                                    Transformation(0, -5.6, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                        },
                        [Firing] = new[] //Firing, Reloading, etc, see Possible Events,  define a new[] for each
                        {
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 20, //number of ticks to complete motion, 60 = 1 second
                                MovementType = ExpoDecay,
                                LinearPoints = new[]
                                {
                                    Transformation(0, 5.6, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 100, //number of ticks to complete motion, 60 = 1 second
                                MovementType = Delay,
                                LinearPoints = new[]
                                {
                                    Transformation(0, 0, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                        },
                        [Reloading] = new[] //Firing, Reloading, etc, see Possible Events,  define a new[] for each
                        {
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 540, //number of ticks to complete motion, 60 = 1 second
                                MovementType = Linear,
                                LinearPoints = new[]
                                {
                                    Transformation(0, -5.6, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                            new RelMove
                            {
                                CenterEmpty = "",
                                EmissiveName = "", //EmissiveName: from above Emissives definitions, TurnOn TurnOff
                                TicksToMove = 540, //number of ticks to complete motion, 60 = 1 second
                                MovementType = Linear,
                                LinearPoints = new[]
                                {
                                    Transformation(0, 5.6, 0), //linear movement
                                },
                                Rotation = Transformation(0, 0, 0), //degrees
                                RotAroundCenter = Transformation(0, 0, 0), //degrees, rotation is around CenterEmpty
                            },
                        },
                    }
                },
            }
            
        };
    }
}
